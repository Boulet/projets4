﻿using System;

namespace Projet
{
    partial class Program
    {   
        static void Main(string[] args)
        {           
            connexion();

            string trame;
            trame = recuperer_la_trame();

            Carte map = new Carte();
            map = traitement_de_la_trame(trame);
            map = ranger_les_cellules_dans_des_parcelles(map);
            map.determiner_la_taille_des_parcelles();
            int[] liste_tailles = map.get_tailles_parcelles();

            map.afficher_la_carte();

            Ia IA = new Ia();
            map = IA.premier_coup(map);

            envoie_de_donnees(IA.get_jeu_IA_pour_serveur());

            bool jeuFini=false;

            while(!jeuFini){
                String reponse = recuperer_la_reponse();

                reponse = recuperer_la_reponse();

                jeuFini = tour_ennemie(reponse);
                
                if(!jeuFini){
                    map.set_coup_precedent(reponse);
                    reponse = recuperer_la_reponse();

                    jeuFini = fin_du_tour_ennemie(reponse);
                    if(jeuFini){
                        recuperer_le_score();
                    }else{
                        map = IA.joue(map);
                        envoie_de_donnees(IA.get_jeu_IA_pour_serveur());
                    }
                }else{
                    recuperer_le_score();
                }
            }
            deconnexion();
        }
    }
}
