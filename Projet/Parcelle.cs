using System;

namespace Projet
{
    class Parcelle
    {
        protected int numero_de_parcelle;
        protected Cellule[] tab_cellules = new Cellule[6];

        protected int taille;

        protected bool parcelle_est_a_nous=false;

        protected bool parcelle_est_a_nous_definitif = false;

        protected bool parcelle_est_a_ennemie=false;

        protected bool est_habitee=false;

        protected bool est_neutre=true;

        public Parcelle() //constucteur utilisé
        {
            this.numero_de_parcelle = 0;
            for(int i=0; i<6; i++)
            {
                tab_cellules[i] = new Cellule();
            }
        }

        public Parcelle(int numero_de_parcelle) // ce constructeur n'a plus l'air de servir
        {
            this.numero_de_parcelle = numero_de_parcelle;
            for (int i = 0; i < 6; i++)
            {
                tab_cellules[i] = new Cellule();
            } 
        }

        public void a_qui_est_cette_parcelle(){
            int nombre_de_cellule_a_nous=0;
            int nombre_de_cellule_a_ennemie=0;
            foreach(Cellule cellule_seule in this.tab_cellules){
                if( cellule_seule.get_nous_appartient()){
                    nombre_de_cellule_a_nous+=1;
                }
                if(cellule_seule.get_appartient_ennemie()){
                    nombre_de_cellule_a_ennemie+=1;
                }              
            }
            if(nombre_de_cellule_a_nous>nombre_de_cellule_a_ennemie){
                this.set_parcelle_est_a_nous(true);
                this.set_parcelle_est_a_ennemie(false);
                this.set_est_neutre(false);
                this.set_est_deja_habitee(true);
            }
            if(nombre_de_cellule_a_nous<nombre_de_cellule_a_ennemie){
                this.set_parcelle_est_a_nous(false);
                this.set_parcelle_est_a_ennemie(true);
                this.set_est_neutre(false);
                this.set_est_deja_habitee(true);
            }
            if(nombre_de_cellule_a_nous == nombre_de_cellule_a_ennemie){
                this.set_parcelle_est_a_nous(false);
                this.set_parcelle_est_a_ennemie(false);
                this.set_est_neutre(true);
                this.set_est_deja_habitee(true);
            }
            if(nombre_de_cellule_a_nous==0 && nombre_de_cellule_a_ennemie==0){
                this.set_parcelle_est_a_nous(false);
                this.set_parcelle_est_a_ennemie(false);
                this.set_est_neutre(true);
                this.set_est_deja_habitee(false);
                
            }
            if(nombre_de_cellule_a_nous > (this.taille /2)){
                this.set_parcelle_est_a_nous_definitif(true);
            }
        }

        public Cellule[] get_cellules(){
            return this.tab_cellules;
        }

        public Cellule get_cellule(int numero_de_cellule)
        {
            return tab_cellules[numero_de_cellule];
        }
         public int get_taille(){
            return this.taille;
        }

        public bool get_parcelle_est_a_nous(){
            return this.parcelle_est_a_nous;
        }

        public bool get_parcelle_est_a_nous_definitif(){
            return this.parcelle_est_a_nous_definitif;
        }
        
        public bool get_parcelle_est_a_ennemie(){
            return this.parcelle_est_a_ennemie;
        }

        public bool get_est_habitee(){
            return this.est_habitee;
        }

        public bool get_est_neutre(){
            return this.est_neutre;
        }

        public int get_numero_de_parcelle()
        {
            return this.numero_de_parcelle;
        }

        public void set_numero_de_parcelle(int numero_de_parcelle)
        {
            this.numero_de_parcelle = numero_de_parcelle;
        }
        public void set_cellule(Cellule la_cellule,int numero_de_cellule)
        {
            this.tab_cellules[numero_de_cellule] = la_cellule;
        }

        public void set_taille(int taille){
            this.taille = taille;
        }

        private void set_parcelle_est_a_nous(bool est_a_nous){
            this.parcelle_est_a_nous=est_a_nous;
        }

        
        private void set_parcelle_est_a_nous_definitif(bool parcelle_est_a_nous_definitif){
            this.parcelle_est_a_nous_definitif = parcelle_est_a_nous_definitif;
        }
        private void set_parcelle_est_a_ennemie(bool est_a_ennemie){
            this.parcelle_est_a_ennemie=est_a_ennemie;
        }
       
        private void set_est_deja_habitee(bool est_habitee){
            this.est_habitee=est_habitee;
        }

        private void set_est_neutre(bool est_neutre){
            this.est_neutre=est_neutre;
        }

    }
    
}
