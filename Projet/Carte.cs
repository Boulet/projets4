using System;

namespace Projet
{
    class Carte
    {

        protected Cellule[,] tab_cellules = new Cellule[10,10];
        protected Parcelle[] tab_parcelles = new Parcelle[17];

        protected int[] liste_tailles = new int[17]; 

        protected Cellule coup_precedent = new Cellule();

        /*
        *Constructeur
        */
        public Carte()
        {
            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++){
                for (int indice_colone = 0; indice_colone <10; indice_colone++) {
                    tab_cellules[indice_ligne,indice_colone] = new Cellule(indice_ligne,indice_colone);
                }
            }
            for (int indice_parcelle = 0; indice_parcelle < 17; indice_parcelle++) {
                tab_parcelles[indice_parcelle] = new Parcelle(); 
            }
        }

        //Getter
        public Cellule get_tab_cellules(int x, int y)
        {
            return tab_cellules[x,y];
        }
        public Parcelle get_parcelle(int numero_de_parcelle)
        {
            return tab_parcelles[numero_de_parcelle];
        }

        public Parcelle[] get_parcelles(){
            return this.tab_parcelles;
        }

        public int[] get_tailles_parcelles(){
            return this.liste_tailles;
        }

        public void set_coup_precedent(string coup){
            string coup_x = coup[2].ToString();
            string coup_y = coup[3].ToString();
            this.coup_precedent.set_x(int.Parse(coup_x));
            this.coup_precedent.set_y(int.Parse(coup_y)); 
            this.coup_precedent.set_numero_de_parcelle_de_la_cellule(trouver_quelle_est_sa_parcelle());
            this.get_tab_cellules(int.Parse(coup_x),int.Parse(coup_y)).set_est_une_case_jouable(false);
            this.get_tab_cellules(int.Parse(coup_x),int.Parse(coup_y)).set_appartient_ennemie(true);
        }

        public Cellule get_coup_precedent(){
            return this.coup_precedent;
        }

        private int trouver_quelle_est_sa_parcelle(){
            int numero_de_parcelle=0;
            numero_de_parcelle = this.get_tab_cellules(this.coup_precedent.get_x(),this.coup_precedent.get_y()).get_numero_de_parcelle_de_la_cellule();
            return numero_de_parcelle;
        }
        
        /*
        *Cette fonction determine la taille des différentes parcelles
        * qui constituent la carte
        */
        public void determiner_la_taille_des_parcelles(){
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    switch (this.get_tab_cellules(i, j).get_numero_de_parcelle_de_la_cellule())
                    {
                        case 0:
                            set_liste_tailles(0,get_liste_tailles()[0]+1);
                            break;
                        case 1:
                            set_liste_tailles(1,get_liste_tailles()[1]+1);
                            break;
                        case 2:
                            set_liste_tailles(2,get_liste_tailles()[2]+1);
                            break;
                        case 3:
                            set_liste_tailles(3,get_liste_tailles()[3]+1);
                            break;
                        case 4:
                            set_liste_tailles(4,get_liste_tailles()[4]+1);
                            break;
                        case 5:
                            set_liste_tailles(5,get_liste_tailles()[5]+1);
                            break;
                        case 6:
                            set_liste_tailles(6,get_liste_tailles()[6]+1);
                            break;
                        case 7:
                            set_liste_tailles(7,get_liste_tailles()[7]+1);
                            break;
                        case 8:
                            set_liste_tailles(8,get_liste_tailles()[8]+1);
                            break;
                        case 9:
                            set_liste_tailles(9,get_liste_tailles()[9]+1);
                            break;
                        case 10:
                            set_liste_tailles(10,get_liste_tailles()[10]+1);
                            break;
                        case 11:
                            set_liste_tailles(11,get_liste_tailles()[11]+1);
                            break;
                        case 12:
                            set_liste_tailles(12,get_liste_tailles()[12]+1);
                            break;
                        case 13:
                            set_liste_tailles(13,get_liste_tailles()[13]+1);
                            break;
                        case 14:
                            set_liste_tailles(14,get_liste_tailles()[14]+1);
                            break;
                        case 15:
                            set_liste_tailles(15,get_liste_tailles()[15]+1);
                            break;
                        case 16:
                            set_liste_tailles(16,get_liste_tailles()[16]+1);
                            break;
                        case -1:
                            this.get_tab_cellules(i,j).set_est_une_case_jouable(false);                
                            break;
                    }
                }
            }
            for(int i=0;i<=16;i++){ // On met les tailles des parcelles à jour.
                this.get_parcelle(i).set_taille(get_liste_tailles()[i]);
            }
        }

        /*
        *Cette fonction permet d'afficher la carte dans la console
        */
        public void afficher_la_carte(){
            String ma_ligne = "";
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    switch (this.get_tab_cellules(i, j).get_numero_de_parcelle_de_la_cellule())
                    {
                        case 0:
                            ma_ligne = ma_ligne + "a ";
                            break;
                        case 1:
                            ma_ligne = ma_ligne + "b ";
                            break;
                        case 2:
                            ma_ligne = ma_ligne + "c ";
                            break;
                        case 3:
                            ma_ligne = ma_ligne + "d ";
                            break;
                        case 4:
                            ma_ligne = ma_ligne + "e ";
                            break;
                        case 5:
                            ma_ligne = ma_ligne + "f ";
                            break;
                        case 6:
                            ma_ligne = ma_ligne + "g ";
                            break;
                        case 7:
                            ma_ligne = ma_ligne + "h ";
                            break;
                        case 8:
                            ma_ligne = ma_ligne + "i ";
                            break;
                        case 9:
                            ma_ligne = ma_ligne + "j ";
                            break;
                        case 10:
                            ma_ligne = ma_ligne + "k ";
                            break;
                        case 11:
                            ma_ligne = ma_ligne + "l ";
                            break;
                        case 12:
                            ma_ligne = ma_ligne + "m ";
                            break;
                        case 13:
                            ma_ligne = ma_ligne + "n ";
                            break;
                        case 14:
                            ma_ligne = ma_ligne + "o ";
                            break;
                        case 15:
                            ma_ligne = ma_ligne + "p ";
                            break;
                        case 16:
                            ma_ligne = ma_ligne + "q ";
                            break;
                        case -1:
                            int valeur = this.get_tab_cellules(i, j).get_valeur();
                            if (valeur >= 64)
                            {
                                ma_ligne = ma_ligne + "M "; // Symbolysation d'une case Mer
                            }
                            else
                            {
                                ma_ligne = ma_ligne + "F ";// Symbolisation d'une case For�t
                            }
                            
                            break;
                    }
                }
                Console.WriteLine(ma_ligne);
                ma_ligne = "";
            }
        }

        private void set_liste_tailles(int i,int taille){
            this.liste_tailles[i]=taille;
        }
        private int[] get_liste_tailles(){
            return this.liste_tailles;
        }
        
    }
}
