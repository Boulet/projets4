using System;
using System.Collections.Generic;

namespace Projet
{
    class Ia
    {   
        // -1- Notre IA ne doit pas jouer sur la même parcelle que le coup précédent 
        // -2- Notre IA ne doit pas jouer sur la même ligne ou la même colone 
        // -3- Notre IA doit chercher à coller un maximum ses graines pour avoir la 
        //      plus grande étendue tout en jouant dans les plus grands territoires possibles
        // -4- Notre IA doit essayer de toujours avoir +de 50% de possession sur une même parcelle.
        // D'apres certains calculs il est plus rentable de viser les parcelles de 2 et de 4 
        private string jeu="11";
        private List<Cellule> liste_des_choix = new List<Cellule>();

        private List<Cellule> liste_pour_premier_coup = new List<Cellule>();
        
        /*
        * Entrée : La carte (map)
        * Sortie : La carte (map) avec le premier coup
        * L'IA determine le premier coup qu'elle veut jouer
        */
        public Carte premier_coup(Carte map){
                remplir_la_liste(map);
                int taille = get_liste_pour_premier_coup().Count;
                Random aleatoire = new Random();
                int le_random = aleatoire.Next(taille);
                Cellule choix = get_liste_pour_premier_coup()[le_random];
                string jeu = ""+choix.get_x()+choix.get_y();
                this.set_jeu_IA(jeu);
                string x = jeu[0].ToString();
                string y = jeu[1].ToString();
                map.get_tab_cellules(int.Parse(x),int.Parse(y)).set_est_une_case_jouable(false);
                map.get_tab_cellules(int.Parse(x),int.Parse(y)).set_nous_appartient(true);
                    
            return map;
        }

        /*
        * Entrée : La carte (map) 
        * Sortie : La carte (map) avec une cellule en plus nous appartenant
        * L'IA choisit la cellule dans laquelle elle veut jouer
        */
        public Carte joue(Carte map){
            determiner_ou_jouer(map);

            trier_les_parcelles(map);

            retirer_les_cases_injouables(map);

            classer_les_choix(map);

            string jeu = ""+get_liste_des_choix()[0].get_x()+get_liste_des_choix()[0].get_y();

            this.set_jeu_IA(jeu);
            string x = jeu[0].ToString();
            string y = jeu[1].ToString();
            map.get_tab_cellules(int.Parse(x),int.Parse(y)).set_est_une_case_jouable(false);
            map.get_tab_cellules(int.Parse(x),int.Parse(y)).set_nous_appartient(true);

            return map;
        }

        /*
        * Entrée : La carte (map)
        * Cette fonction retourne une liste de cellule appartenant à des parcelles de taille 2
        */
        private void remplir_la_liste(Carte map){
            List<Cellule> liste_locale = new List<Cellule>();
            for(int i=0;i<=9;i++)
            {
                for(int j=0;j<=9;j++)
                {
                    if(map.get_tab_cellules(i,j).get_est_une_case_jouable() && map.get_parcelle(map.get_tab_cellules(i,j).get_numero_de_parcelle_de_la_cellule()).get_taille()==2){
                        liste_locale.Add(map.get_tab_cellules(i,j));
                    }
                }
            }
            set_liste_pour_premier_coup(liste_locale);
        }

        private void classer_les_choix(Carte map){
            List<Cellule> liste_locale = new List<Cellule>();
            int taille = get_liste_des_choix().Count;
            int[] tableau_des_scores = new int[taille];
            Cellule[] tableau_des_cellules = new Cellule[taille];
            for(int i=0;i<taille;i++)
            {
                tableau_des_cellules[i]=get_liste_des_choix()[i];
                tableau_des_scores[i]=calculer_score_de_cellule(tableau_des_cellules[i], map);
            }
            for(int i=taille-1;i>=0;i--)
            {
                for(int j=taille-2;j>=0;j--)
                {
                    if(tableau_des_scores[j]<=tableau_des_scores[j+1])
                    {
                        Cellule cellule_memoire=tableau_des_cellules[j+1];
                        int score_memoire=tableau_des_scores[j+1];
                        tableau_des_scores[j+1]=tableau_des_scores[j];
                        tableau_des_scores[j]=score_memoire;
                        tableau_des_cellules[j+1]=tableau_des_cellules[j];
                        tableau_des_cellules[j]=cellule_memoire;
                    }
                }
            }
            for(int i=0;i<taille;i++)
            {
                liste_locale.Add(tableau_des_cellules[i]);
            }
            set_liste_des_choix(liste_locale);
        }

        private int calculer_score_de_cellule(Cellule la_cellule,Carte map)
        {
            int score=0;
            score += regarder_autour(la_cellule, map);
            score += parcelle_deja_habitee(la_cellule,map);
            score += meilleure_parcelle(la_cellule,map);
            return score;
        }
        private int meilleure_parcelle(Cellule la_cellule, Carte map)
        {
            int score = 0;
            if(map.get_parcelle(la_cellule.get_numero_de_parcelle_de_la_cellule()).get_taille()==2)
            {
                score = 5;
            }
            if(map.get_parcelle(la_cellule.get_numero_de_parcelle_de_la_cellule()).get_taille()==3)
            {
                score = 10;
            }
            if(map.get_parcelle(la_cellule.get_numero_de_parcelle_de_la_cellule()).get_taille()==4)
            {
                score = 5;
            }
            if(map.get_parcelle(la_cellule.get_numero_de_parcelle_de_la_cellule()).get_taille()==6)
            {
                score = 10;
            }

            return score;
        }
        private int parcelle_deja_habitee(Cellule la_cellule, Carte map)
        {
            int score=0;
            map.get_parcelle(la_cellule.get_numero_de_parcelle_de_la_cellule()).a_qui_est_cette_parcelle();

            if(map.get_parcelle(la_cellule.get_numero_de_parcelle_de_la_cellule()).get_est_habitee()==false)
            {
                score=10;
            }
            if(map.get_parcelle(la_cellule.get_numero_de_parcelle_de_la_cellule()).get_est_neutre()==true)
            {
                score=12;
            }
            if(map.get_parcelle(la_cellule.get_numero_de_parcelle_de_la_cellule()).get_parcelle_est_a_nous()==true)
            {
                score=8;
            }
            if(map.get_parcelle(la_cellule.get_numero_de_parcelle_de_la_cellule()).get_parcelle_est_a_nous_definitif()==true)
            {
                score = -8;
            }
            return score;
        }

        private int regarder_autour(Cellule la_cellule, Carte map)
        {
            int score=0;
            if(la_cellule.get_x()<=8 && map.get_tab_cellules(la_cellule.get_x()+1,la_cellule.get_y()).get_nous_appartient())//Si la cellule de droite est à nous
            {
                score +=15;
            }
            if(la_cellule.get_x()>=1 && map.get_tab_cellules(la_cellule.get_x()-1,la_cellule.get_y()).get_nous_appartient())//Si la cellule de droite est à nous
            {
                score +=15;
            }
            if(la_cellule.get_y()<=8 && map.get_tab_cellules(la_cellule.get_x(),la_cellule.get_y()+1).get_nous_appartient())//Si la cellule de droite est à nous
            {
                score +=15;
            }
            if(la_cellule.get_y()>=1 && map.get_tab_cellules(la_cellule.get_x(),la_cellule.get_y()-1).get_nous_appartient())//Si la cellule de droite est à nous
            {
                score +=15;
            }
            return score;
        }

        private void retirer_les_cases_injouables(Carte map)
        {
            List<Cellule> liste_locale = new List<Cellule>();

            foreach(Cellule cellule_seule in get_liste_des_choix())
            {
                if( map.get_tab_cellules(cellule_seule.get_x(),cellule_seule.get_y()).get_est_une_case_jouable()!=false)
                {
                    liste_locale.Add(cellule_seule);
                }              
            }
            set_liste_des_choix(liste_locale);
        }
        private void trier_les_parcelles(Carte map)
        {
            List<Cellule> liste_locale = new List<Cellule>();
            foreach(Cellule cellule_seule in get_liste_des_choix())
            {
                if(map.get_coup_precedent().get_numero_de_parcelle_de_la_cellule()!= map.get_tab_cellules(cellule_seule.get_x(),cellule_seule.get_y()).get_numero_de_parcelle_de_la_cellule())
                {
                    cellule_seule.set_numero_de_parcelle_de_la_cellule(map.get_tab_cellules(cellule_seule.get_x(),cellule_seule.get_y()).get_numero_de_parcelle_de_la_cellule());
                    liste_locale.Add(cellule_seule);
                }
            }
            set_liste_des_choix(liste_locale);
        }

        private void determiner_ou_jouer(Carte map)
        {
            List<Cellule> liste = new List<Cellule>();
            Cellule coup_precedent = map.get_coup_precedent();
            int ligne = coup_precedent.get_x();
            int colonne = coup_precedent.get_y();

            //On aura d'abord toute la colonne puis toute la ligne dans liste
            for(int i=0;i<=9;i++)
            {
                if(ligne!=i)
                {
                    liste.Add(new Cellule(i,colonne));
                }
            }
            for(int i=0;i<=9;i++)
            {
                if(colonne!=i)
                {
                    liste.Add(new Cellule(ligne,i));
                }
            }
            set_liste_des_choix(liste);
        }

        public string get_jeu_IA()
        {
            return this.jeu;
        }

        public string get_jeu_IA_pour_serveur()
        {
            return "A:"+this.jeu;
        }

        private void set_jeu_IA(string jeu)
        {
            this.jeu = jeu;
        }

        private void set_liste_des_choix(List<Cellule> liste_des_choix)
        {
            this.liste_des_choix = liste_des_choix;
        }

        private void set_liste_pour_premier_coup(List<Cellule> liste_pour_premier_coup)
        {
            this.liste_pour_premier_coup = liste_pour_premier_coup;
        }

        private List<Cellule> get_liste_des_choix()
        {
            return this.liste_des_choix;
        }

        private List<Cellule> get_liste_pour_premier_coup()
        {
            return this.liste_pour_premier_coup;
        }
    }
}
